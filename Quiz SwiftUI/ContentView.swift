//
//  ContentView.swift
//  Quiz SwiftUI
//
//  Created by Chris Downie on 7/29/20.
//  Copyright © 2020 Chris Downie. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var selectedIndex = 0
    @State var isAnswerShown = false
    
    let questions: [String] = [
        "What is 7+7?",
        "What is the capital of Vermont?",
        "What is cognac made from?"
    ]
    let answers: [String] = [
        "14",
        "Montpelier",
        "Grapes"
    ]
    
    var body: some View {
        VStack {
            Spacer()
            Text(questions[selectedIndex])
                .font(.largeTitle)
                .multilineTextAlignment(.center)

            Button(action: {
                if self.selectedIndex == self.questions.count - 1 {
                    self.selectedIndex = 0
                } else {
                    self.selectedIndex += 1
                }
                self.isAnswerShown = false
            }) {
                Text("Next Question")
            }.padding()
            
            Spacer()
            
            Text(isAnswerShown ? answers[selectedIndex] : "???")
                .font(.title)
                .foregroundColor(.purple)
                .italic()
            
            Button(action: {
                self.isAnswerShown = true
            }) {
                Text("Show Answer")
            }.padding()
            
            Spacer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
